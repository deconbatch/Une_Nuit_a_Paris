* Une Nuit a Paris.
One of the creative coding works by deconbatch.
[[https://www.deconbatch.com/2020/02/une-nuit-paris.html]['Une Nuit a Paris.' on Deconbatch's Creative Coding : Examples with Code.]]

[[./example01.png][An example image of beautiful colored triangle shapes.]]

** Description of this creative coding.
   - It's a creative coding artwork made with Processing. It draws beautiful colored triangle shapes.
   - The key to drawing this beautiful gradation in the triangle is just this.
     :  size(900, 900, P2D);
[[./d.normal.png][size(900, 900);]]
[[./d.p2d.png][size(900, 900, P2D);]]
   - Processing Reference says that P2D will use OpenGL-compatible graphics hardware. I don't know this code will generate the same gradation with other environments.
[[https://processing.org/reference/size_.html][size() \ Language (API) \ Processing 3+]]
   - This code does not display any images on the screen but generates image files in frames directory.
** Change log.
   - created : 2020/02/29
