/**
 * Une Nuit a Paris.
 * It draws beautiful colored triangle shapes.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.02.29
 */

void setup() {
  size(900, 900, P2D);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth(8);
  noLoop();
}

void draw() {

  int shpMax = 4;
  
  translate(width * 0.5, height * 0.5);
  background(0.0, 0.0, 90.0, 100.0);
  fill(0.0, 0.0, 100.0, 100.0);
  noStroke();
  rect(0.0, 0.0, width * 0.9, width * 0.9);

  for(int fileCnt = 3; fileCnt >= 1; fileCnt--) {

    // shape parameters
    float baseSat = 0.0;
    float baseBri = 0.0;
    int   pathMax = 0;
    int   pathLen = 0;
    if (fileCnt == 3) {
      baseSat = 40.0;
      baseBri = 90.0;
      pathMax = 50;
      pathLen = floor(width * 0.44);
    } else if (fileCnt == 2) {
      baseSat = 60.0;
      baseBri = 90.0;
      pathMax = 30;
      pathLen = floor(width * 0.22);
    } else {
      baseSat = 80.0;
      baseBri = 80.0;
      pathMax = 20;
      pathLen = floor(width * 0.11);
    }
    
    for(int shpCnt = 0; shpCnt < shpMax; shpCnt++) {
      // start from center
      int pX = 0;
      int pY = 0;

      beginShape(TRIANGLE_STRIP);
      strokeWeight(1.0);
      stroke(0.0, 0.0, 100.0, 100.0);
      // random walk
      for(int pathCnt = 0; pathCnt < pathMax; pathCnt++) {
        float direction = random(-1.0, 1.0);
        direction = direction == 0 ? 1.0 : direction / abs(direction);
        if (random(1.0) < 0.5) {
          pX += direction * pathLen;
        } else {
          pY += direction * pathLen;
        }
        if (abs(pX) >= width * 0.5) {
          pX -= direction * pathLen;
        }
        if (abs(pY) >= height * 0.5) {
          pY -= direction * pathLen;
        }
        fill(random(360.0), baseSat, baseBri, 100.0);
        vertex(pX, pY);
      }  
      endShape();

      casing();

    }
    saveFrame("frames/" + String.format("%04d", fileCnt) + ".png");
  }
  exit();
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(20.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(18.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}
